package com.lionel.claudon.android.app.quizapplication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import com.lionel.claudon.android.app.quizapplication.model.Question;

import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {
    private final static String LOG_TAG = MainActivity.class.getSimpleName();

    private static int NUMBER_OF_QUESTIONS_STEP = 10;
    private static int MIN_NUMBER_OF_QUESTIONS = 10;

    private TextView numberOfQuestionValueTextView;
    private SeekBar numberOfQuestionsSeekbar;
    private List<Question> questions;


    public MainActivityFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        this.numberOfQuestionsSeekbar = (SeekBar) rootView.findViewById(R.id.seekBar);
        this.numberOfQuestionValueTextView = (TextView) rootView.findViewById(R.id.numberOfQuestionsValueTextView);

        this.numberOfQuestionsSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.i(LOG_TAG, "Change not from user");
                int progress = seekBar.getProgress();
                // Set value to closest 10-multiplicator
                int rounded = Math.round((float) progress / NUMBER_OF_QUESTIONS_STEP) * NUMBER_OF_QUESTIONS_STEP;
                if (rounded > seekBar.getMax()) {
                    rounded = seekBar.getMax();
                } else if (rounded == 0) {
                    rounded = Math.min(MIN_NUMBER_OF_QUESTIONS, MainActivityFragment.this.questions.size());
                }
                Log.i(LOG_TAG, "Progress of the number of question seek bar: " + progress + " Setting value " + rounded);
                seekBar.setProgress(rounded);
                numberOfQuestionValueTextView.setText(String.valueOf(rounded));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser) {
                    int rounded = Math.round((float) progress / NUMBER_OF_QUESTIONS_STEP) * NUMBER_OF_QUESTIONS_STEP;
                    if (rounded > seekBar.getMax()) {
                        rounded = seekBar.getMax();
                    } else if (rounded == 0) {
                        rounded = Math.min(MIN_NUMBER_OF_QUESTIONS, MainActivityFragment.this.questions.size());
                    }
                    numberOfQuestionValueTextView.setText(String.valueOf(rounded));
                }
            }
        });

        return rootView;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;

        if (questions != null) {
            if (questions.size() < MIN_NUMBER_OF_QUESTIONS) {
                MIN_NUMBER_OF_QUESTIONS = 1;
                NUMBER_OF_QUESTIONS_STEP = 1;
            }

            int initialNumberOfQuestions = questions.size() / 3 - (questions.size() / 3) % NUMBER_OF_QUESTIONS_STEP == 0 ?
                    Math.min(MIN_NUMBER_OF_QUESTIONS, questions.size()) : questions.size() / 3 - (questions.size() / 3) % NUMBER_OF_QUESTIONS_STEP;
            this.numberOfQuestionsSeekbar.setMax(questions.size());
            this.numberOfQuestionsSeekbar.setProgress(initialNumberOfQuestions);
            this.numberOfQuestionValueTextView.setText(String.valueOf(this.numberOfQuestionsSeekbar.getProgress()));
        }
    }

    public int getNumberOfQuestionsInQuiz() {
        return this.numberOfQuestionsSeekbar.getProgress();
    }

}
