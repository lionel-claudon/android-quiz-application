package com.lionel.claudon.android.app.quizapplication;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by lionel on 09/10/15.
 */
public class AnswerAdapter extends ArrayAdapter<String> {
    private final static String LOG_TAG = AnswerAdapter.class.getSimpleName();

    private Map<String, Boolean> userAnswersList;
    private List<String> validAnswers;
    private boolean showResults = false;

    public AnswerAdapter(Context context, int textViewResourceId,
                           List<String> answers) {
        super(context, textViewResourceId, answers);
        this.userAnswersList = new HashMap<>();
        this.validAnswers = new ArrayList<>();
    }

    public void userClickedAt(int position) {
        String s = (String) userAnswersList.keySet().toArray()[position];
        userAnswersList.put(s, !userAnswersList.get(s));
        notifyDataSetChanged();
    }

    public void setData(List<String> answers, List<String> validAnswers) {
        clear();
        this.userAnswersList.clear();
        this.validAnswers.clear();
        int i = 0;
        for(String s : answers) {
            this.userAnswersList.put(s, false);
            insert(s, i);
            i++;
        }
        this.validAnswers.addAll(validAnswers);
        notifyDataSetChanged();
    }

    public void setShowResults(boolean showResults) {
        this.showResults = showResults;
        notifyDataSetChanged();
    }

    public boolean isUserCorrect() {
        if(getNumberOfUserAnswers() == validAnswers.size()) {
            for(String s : getUserAnswers()) {
                if(!doestListStrictlyContain(validAnswers, s)) {
                    Log.i(LOG_TAG, "User answered an invalid answer - incorrect");

                    return false;
                }
            }
        } else {
            Log.i(LOG_TAG, "User has not answered the right amount of questions - incorrect");
            return false;
        }

        return true;
    }

    private class ViewHolder {
        TextView answerText;
        CheckBox cb;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.answer_item_layout, null);

            holder = new ViewHolder();
            holder.answerText = (TextView) convertView.findViewById(R.id.code);
            holder.cb = (CheckBox) convertView.findViewById(R.id.checkBox1);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        String answer = (String) userAnswersList.keySet().toArray()[position];
        holder.answerText.setText(answer);
        holder.cb.setChecked(userAnswersList.get(answer));
        holder.cb.setTag(answer);
        holder.answerText.setTag(answer);

        if(showResults) {
            if(doestListStrictlyContain(this.validAnswers, answer)) {
                convertView.setBackgroundColor(getContext().getResources().getColor(R.color.valid_answer));
            } else if(userAnswersList.get(answer) && !doestListStrictlyContain(this.validAnswers, answer)) {
                convertView.setBackgroundColor(getContext().getResources().getColor(R.color.incorrect_answer));
            } else {
                convertView.setBackgroundColor(Color.TRANSPARENT);
            }
        } else {
            convertView.setBackgroundColor(Color.TRANSPARENT);
        }

        return convertView;
    }

    private List<String> getUserAnswers() {
        List<String> res = new ArrayList<>();
        for(String s : userAnswersList.keySet()) {
            if(userAnswersList.get(s)) res.add(s);
        }

        return res;
    }

    public int getNumberOfUserAnswers() {
        return getUserAnswers().size();
    }

    public Map<String, Boolean> getUserAnswersList() {
        return userAnswersList;
    }

    private boolean doestListStrictlyContain(List<String> list, String s) {
        boolean res = false;

        for(String ls : list) {
            if(ls.equals(s)) res = true;
        }

        return res;
    }
}