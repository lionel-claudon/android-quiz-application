package com.lionel.claudon.android.app.quizapplication.model;

import java.io.Serializable;

/**
 * Created by lionel on 07/10/15.
 */
public class Question implements Serializable {
    private int id;
    private String question;
    private String[] answers;
    private int[] validAnswers;

    public Question(int id, String question, String[] answers, int[] validAnswers) {
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.validAnswers = validAnswers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public int[] getValidAnswers() {
        return validAnswers;
    }

    public void setValidAnswers(int[] validAnswers) {
        this.validAnswers = validAnswers;
    }
}
