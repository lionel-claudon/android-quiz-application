package com.lionel.claudon.android.app.quizapplication;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.lionel.claudon.android.app.quizapplication.model.Question;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private final static String LOG_TAG = MainActivity.class.getSimpleName();

    private List<Question> questions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent i = getIntent();
        if(this.questions == null) {
            if (i.getSerializableExtra("questions") == null) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle(R.string.error_input_questions_title);
                alert.setMessage(R.string.error_input_questions);
                alert.setPositiveButton("Ok", null);
                alert.show();
            } else {
                this.questions = (List<Question>) i.getSerializableExtra("questions");
            }
        }

        MainActivityFragment mainFragment = ((MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_main));
        mainFragment.setQuestions(this.questions);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return false;
    }

    public void onStartQuizButtonClicked(View view) {
        MainActivityFragment mainFragment = ((MainActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_main));
        int numberOfQuestion = mainFragment.getNumberOfQuestionsInQuiz();

        Map<Integer, Question> quizQuestions = new HashMap<>();
        while(quizQuestions.size() < numberOfQuestion) {
            int r = (int) (Math.random()*this.questions.size());
            Question q = this.questions.get(r);
            if(!quizQuestions.containsKey(q.getId())) {
                quizQuestions.put(q.getId(), q);
            }
        }

        Intent intent = new Intent(this, QuizActivity.class);
        intent.putExtra("quizQuestions", (Serializable) quizQuestions);
        startActivity(intent);
    }
}
