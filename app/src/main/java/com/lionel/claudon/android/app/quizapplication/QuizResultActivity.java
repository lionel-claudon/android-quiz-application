package com.lionel.claudon.android.app.quizapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class QuizResultActivity extends AppCompatActivity {
    private TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz_result);

        this.resultTextView = (TextView) findViewById(R.id.resultTextView);
        Intent i = getIntent();
        float result = (float) i.getSerializableExtra("score");

        resultTextView.setText(String.valueOf(Math.round(result)) + " %");

        if(result<70) {
            resultTextView.setTextColor(getResources().getColor(R.color.failed_quiz));
        } else {
            resultTextView.setTextColor(getResources().getColor(R.color.passed_quiz));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }

    public void onOkButtonPressed(View view) {
        // Back to Main Menu
       finish();
    }
}
