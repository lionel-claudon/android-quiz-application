package com.lionel.claudon.android.app.quizapplication;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.lionel.claudon.android.app.quizapplication.model.Question;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuizActivity extends AppCompatActivity {
    private Map<Integer,Question> quizQuestions;
    private TextView percentOfQuizDoneTextView;
    private TextView questionTextView;
    private ProgressBar quizProgressBar;
    private AnswerAdapter answerAdapter;
    private Button validateAnswersButton;
    private Button nextQuestionButton;
    private ListView answersListView;
    private AdapterView.OnItemClickListener listViewClickListener;
    private int totalNumberOfQuestions = 0;
    private float score = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        this.questionTextView = (TextView) findViewById(R.id.questionTextView);
        this.quizProgressBar = (ProgressBar) findViewById(R.id.doneQuizProgressBar);
        this.validateAnswersButton = (Button) findViewById(R.id.validateAnswersButton);
        this.nextQuestionButton = (Button) findViewById(R.id.nextQuestionButton);
        this.percentOfQuizDoneTextView = (TextView) findViewById(R.id.percentDoneQuizTextView);

        if(quizQuestions == null) {
            Intent i = getIntent();
            if (i.getSerializableExtra("quizQuestions") != null) {
                this.quizQuestions = (HashMap<Integer, Question>) i.getSerializableExtra("quizQuestions");
                totalNumberOfQuestions = quizQuestions.size();
            }

            this.answerAdapter = new AnswerAdapter(this,
                    R.layout.answer_item_layout, new ArrayList<String>());
            answersListView = (ListView) findViewById(R.id.questionListView);
            // Assign adapter to ListView
            answersListView.setAdapter(answerAdapter);
            listViewClickListener = new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
                    answerAdapter.userClickedAt(position);
                    if(answerAdapter.getNumberOfUserAnswers() > 0) {
                        QuizActivity.this.validateAnswersButton.setEnabled(true);
                    } else {
                        QuizActivity.this.validateAnswersButton.setEnabled(false);
                    }
                }
            };

            answersListView.setOnItemClickListener(listViewClickListener);
        }

        resumeQuiz();
    }

    private void resumeQuiz() {
        Question q = quizQuestions.get(quizQuestions.keySet().toArray()[0]);
        List<String> validAnswers = new ArrayList<>();
        for(int i : q.getValidAnswers()) {
            validAnswers.add(q.getAnswers()[i]);
        }
        this.answerAdapter.setData(Arrays.asList(q.getAnswers()), validAnswers);
        questionTextView.setText(q.getQuestion());
        int progress = Math.round(100 * (1 - ((float) quizQuestions.size()) / totalNumberOfQuestions));
        quizProgressBar.setProgress(progress);
        percentOfQuizDoneTextView.setText(String.valueOf(progress) + "%");

        if(answerAdapter.getNumberOfUserAnswers() > 0) {
            this.validateAnswersButton.setEnabled(true);
        } else {
            this.validateAnswersButton.setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return false;
    }

    public void onValidateAnswersButtonClicked(View view) {
        //Validate answers
        this.answerAdapter.setShowResults(true);
        this.answersListView.setOnItemClickListener(null);
        if(this.answerAdapter.isUserCorrect()) {
            score++;
        }

        //calculate score
        this.nextQuestionButton.setVisibility(View.VISIBLE);
        this.validateAnswersButton.setEnabled(false);
    }

    public void onNextQuestionButtonClicked(View view) {
        this.answerAdapter.setShowResults(false);
        this.answersListView.setOnItemClickListener(listViewClickListener);
        this.quizQuestions.remove((quizQuestions.keySet().toArray()[0]));

        if(this.quizQuestions.size() > 0) {
            resumeQuiz();
            this.nextQuestionButton.setVisibility(View.INVISIBLE);
        } else {
            finish();
            //End of the quiz
            Intent i = new Intent(this, QuizResultActivity.class);
            i.putExtra("score", 100*((float)score)/totalNumberOfQuestions );
            startActivity(i);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.confirm_exit_quiz_message_title);
        alert.setMessage(R.string.confirm_exit_quiz_message);
        alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                QuizActivity.super.onBackPressed();
            }
        });
        alert.setNegativeButton("No", null);
        alert.show();
    }
}
