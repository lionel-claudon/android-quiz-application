package com.lionel.claudon.android.app.quizapplication;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.lionel.claudon.android.app.quizapplication.model.Question;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SplashActivity extends AppCompatActivity {
    private final static String LOG_TAG = SplashActivity.class.getSimpleName();

    private List<Question> questions = new ArrayList<Question>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /**
         * Showing splashscreen while making network calls to download necessary
         * data before launching the app Will use AsyncTask to make http call
         */
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                try {
                    SplashActivity.this.questions = new PrefetchData().execute().get();
                } catch (Exception e) {
                    Log.e(LOG_TAG, "Error while waiting splash task", e);
                }
            }
        }, 1500);
    }

    /**
     * Async Task to make http call
     */
    private class PrefetchData extends AsyncTask<Void, Void, List<Question>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected List<Question> doInBackground(Void... arg0) {
            List<Question> questions = new ArrayList<>();
            InputStream inputStream = getResources().openRawResource(R.raw.questions);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

            int c;
            try {
                c = inputStream.read();
                while (c != -1) {
                    byteArrayOutputStream.write(c);
                    c = inputStream.read();
                }
                inputStream.close();
            } catch (IOException e) {
                Log.e(LOG_TAG, "Error while loading questions input file", e);
                return null;
            }

            Log.v(LOG_TAG, "Questions data read:" + byteArrayOutputStream.toString());
            long t0 = System.currentTimeMillis();

            try {
                // Parse the data into jsonobject to get original data in form of json.
                JSONArray data = new JSONArray(byteArrayOutputStream.toString());

                for (int i = 0; i < data.length(); i++) {
                    JSONObject o = data.getJSONObject(i);
                    int qId = (int) o.get("id");
                    String q = o.getString("question");
                    //json array to string array
                    JSONArray answers = o.getJSONArray("answers");
                    String[] a = new String[answers.length()];
                    for(int k = 0; k<answers.length(); k++) {
                        a[k] = (String) answers.get(k);
                    }

                    //for integers we must parse it
                    JSONArray valids = o.getJSONArray("validAnswers");
                    int[] v = new int[valids.length()];
                    for(int j = 0; j<valids.length(); j++) {
                        v[j] = (int) valids.get(j);
                    }

                    questions.add(new Question(qId, q, a, v));

                }
            } catch (Exception e) {
                Log.e(LOG_TAG, "Error while loading questions input file", e);
                return null;
            }

            Log.i(LOG_TAG, "Processed input file in " + (System.currentTimeMillis() - t0) + " ms");

            return questions;
        }

        @Override
        protected void onPostExecute(List<Question> result) {
            super.onPostExecute(result);
            // After completing http call
            // will close this activity and lauch main activity
            Intent i = new Intent(SplashActivity.this, MainActivity.class);
            i.putExtra("questions", (Serializable) questions);
            startActivity(i);

            // close this activity
            finish();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return false;
    }
}
